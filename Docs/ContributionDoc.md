FSFTN Financial Contribution Document
==============================

Free Software Foundation Tamil Nadu is a Non-Profit Mass Organisation which aims to enlighten the masses with the essence of Free Software, Free Culture and primarily the Freedom of Knowledge. This document primarily focuses on the Guidelines of the Financial Contribution to FSFTN.

Financial Requirements
============================

FSFTN requires Finance for the following activities,

1. Conducting and Organising Camps
2. Manage GLUG Activities all over Tamil Nadu
3. Host and Coordinate other Community Events
4. Office Space Rent
5. Internet for the Office
6. Managing the Organisation Registrational Expenses
7. Organising other Events such as Conferences, Meetings, etc.



Financial Inflow for the Organisation
==========================================

Organisation have some standard Revenue Generating Models inorder to continue and travel towards our goal of Knowledge Freedom. Some of the Current methods are,

1. Yearly Camps
2. College Workshops and Guest Lectures
3. Fund, Donations and Sponsorships

Guidelines
================

FSFTN follows some guidelines regarding their acceptance of Funds from various organisations and institutions. These guidelines also clearly states the donating party's role in the organisation.

1. It accepts donations from the Org/Inst who share common goals with the organisation
2. It accepts funds from the Org/Inst who aim to work with us in the long run
3. It accepts funds from other FOSS Communities who aims to help the movement
4. It accepts funds from the Org/Ins which doesn't clearly have any political affiliation
5. FSFTN does not provide any decision making rights to those who donate to the organisation
6. FSFTN does not get funds from Corporate Institutions who promote Proprietery Tech
7. FSFTN does not get funds from the institutions who influence the programme of the organisation
8. FSFTN clearly provides a transparent Financial Status whenever required by the Organisation
9. FSFTN has the right to discontinue the relationahips with any Org/Ins at any time without the concern of their previous financial contribution
10. Contributions can be per Event, per Camp and also in a long term basis
